package selenide.helpers

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.WebDriverRunner
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebDriver
import selenide.helpers.browsers.Chrome
import selenide.helpers.browsers.ChromeSp
import selenide.helpers.browsers.Firefox
import selenide.helpers.browsers.IE
import selenide.helpers.browsers.Edge
import selenide.helpers.browsers.Safari
import selenide.helpers.browsers.Remote
import java.net.URL

object BrowserUtils {

    val USE_PROXY_FLAG = false
    val USE_HUB_SERVER = false
    val USE_HEADLESS = false

    // private val DEFAULT_PROXY_SERVER: String = "http://localhost:8888"
    private val DEFAULT_PROXY_SERVER: String = "http://192.168.0.6:8888"

    // private val SELENIUM_DEFAULT_HUB_SERVER: String = "http://localhost:4444/wd/hub"
    private val SELENIUM_DEFAULT_HUB_SERVER: String = "http://loalhost:4444/wd/hub"

    // src/mainの場合: ./out/production/resources/drivers
    // src/testの場合: ./out/test/resources/drivers
    private val DRIVER_ROOT_PATH = "build/resources/test/drivers"

    val hubServer: URL = getSeleniumHubServer()
    val proxyServer: URL = getProxyServer()

    /*
        Selenideの構成を行う
        Selenide側のConfigurationを可能な限り使用する方針とする
     */
    fun configureSelenide() {
        // https://github.com/codeborne/selenide/blob/master/src/main/java/com/codeborne/selenide/Configuration.java

        /*
            以下のConfigurationはWebDriverProviderを継承したCustomWebDriverProvider側で設定を行う
            Configuration.baseUrl = ""

            Configuration.headless = false // Default: false, Works only for Chrome(59+) and Firefox(56+).
            Works only for Chrome, Firefox and Opera.

            "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
            Configuration.browserBinary = "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary"

            // Supported values: "chrome", "firefox", "legacy_firefox", "ie", "htmlunit", "phantomjs", "opera", "safari", "edge", "jbrowser"
            Configuration.browser = WebDriverRunner.CHROME // Default value: "firefox"

            Configuration.remote = null // Default value: null (Grid is not used). Set http://localhost:5678/wd/hub" if you use Grid.

            `normal`: return after the load event fires on the new page (it's default in Selenium webdriver);
            `eager`: return after DOMContentLoaded fires;
            `none`: return immediately
            Configuration.pageLoadStrategy = "normal" // Default value: "normal".
         */

        Configuration.collectionsTimeout = 6000 // Default value: 6000 (milliseconds)
        Configuration.timeout = 6000 // Default value: 4000 (milliseconds)
        Configuration.pollingInterval = 500 // Default value: 100 (milliseconds)
        Configuration.collectionsPollingInterval = 200 // Default value: 200 (milliseconds)
        Configuration.holdBrowserOpen = false // Default value: false.
        Configuration.reopenBrowserOnFail = true // Default value: true
        Configuration.openBrowserTimeoutMs = 15000 // Default value: 15000 (milliseconds)
        Configuration.closeBrowserTimeoutMs = 5000 // Default value: 5000 (milliseconds)

        Configuration.browserVersion = null // Default value: none, Which browser version to use (for Internet Explorer).
        Configuration.browserSize = "1024x768" // Default value: none (browser size will not be set explicitly)
        Configuration.startMaximized = false // Default value: true

        Configuration.clickViaJs = false // Default value: false
        Configuration.captureJavascriptErrors = true // Default value: true
        Configuration.screenshots = false // Default value: true
        Configuration.reportsFolder = "build/reports/tests" // Default value: "build/reports/tests" (this is default for Gradle projects)
        Configuration.dismissModalDialogs = false // Default value: false

        Configuration.fastSetValue = false // Default value: false

        Configuration.selectorMode = Configuration.SelectorMode.CSS // Default Selenium behavior is CSS
        Configuration.assertionMode = Configuration.AssertionMode.STRICT // Default mode is STRICT - tests are failing immediately
        Configuration.fileDownload = Configuration.FileDownloadMode.HTTPGET // Default: HTTPGET
        Configuration.driverManagerEnabled = true // Default: true
    }

    /*
        各種ブラウザーを起動する
     */
    fun launchBrowser(browserType: BrowserType = BrowserType.CHROME): WebDriver {
        configureSelenide()

        Configuration.browser = getWebDriver(BrowserType.CHROME)

        // EventListenerを使用する
        // EventFiringWebDriverは使用せずに次のようにリスナーを追加する
        WebDriverRunner.addListener(EventListener())
        /*
            // 以下の方法は使用しない
            val efWebDriver = EventFiringWebDriver(driver)
            val listener = EventListener()
            efWebDriver.register(listener)
            WebDriverRunner.setWebDriver(efWebDriver)
         */

        // ToDo: 動作するかどうか調べる
        // WebDriverRunner.clearBrowserCache()

        return WebDriverRunner.getWebDriver()
    }

    fun closeBrowser(driver: WebDriver?) {
        driver?.quit()
    }

    /*
     *  各WebDriverのCustomWebDriverProviderの名前を返す
     */
    private fun getWebDriver(browserType: BrowserType, isRemote: Boolean = false): String {
        when (browserType) {
            BrowserType.CHROME -> {
                return Chrome.CustomWebDriverProvider::class.java.name
            }
            BrowserType.SMART_PHONE -> {
                return ChromeSp.CustomWebDriverProvider::class.java.name
            }
            BrowserType.FIREFOX -> {
                return Firefox.CustomWebDriverProvider::class.java.name
            }
            BrowserType.IE -> {
                return IE.CustomWebDriverProvider::class.java.name
            }
            BrowserType.EDGE -> {
                return Edge.CustomWebDriverProvider::class.java.name
            }
            BrowserType.SAFARI -> {
                return Safari.CustomWebDriverProvider::class.java.name
            }
        }
    }

    /*
        ChromeとIEのプロキシー設定を得る
     */
    fun getProxy(): Proxy {

        val proxy = Proxy()
        val url = proxyServer.toString()
        proxy.setHttpProxy(url)
                .setFtpProxy(url)
                .setSslProxy(url)

        return proxy
    }

    /*
        任意のキー名の環境変数を取得する
     */
    private fun getEnv(name: String, defaultValue: String): String = if (System.getenv(name).isNullOrEmpty()) defaultValue else System.getenv(name)

    /*
        Selenium GridのHubサーバーのURLを取得する
     */
    private fun getSeleniumHubServer(name: String = "QA_SELENIUM_HUB_SERVER"): URL {
        val url: String = getEnv(name, SELENIUM_DEFAULT_HUB_SERVER)
        return URL(url)
    }

    /*
        ProxyサーバーのURLを取得する
     */
    private fun getProxyServer(name: String = "QA_PROXY_SERVER"): URL {
        val url: String = getEnv(name, DEFAULT_PROXY_SERVER)
        return URL(url)
    }

    /*
        WebDriverファイルのある場所のフルパスを得る
     */
    fun getDriverLocation(browserType: BrowserType): String {

        var operatingSystem = "windows"
        var executableFile = "chromedriver.exe"
        var browserFolder = "chrome"

        // Platformの判定
        var machineBits = "32bit"

        when (browserType) {
            BrowserType.CHROME -> {
                browserFolder = "chrome"
                executableFile = "chromedriver.exe"
                if (CommonUtils.isMac()) {
                    machineBits = "64bit"
                }
            }
            BrowserType.SMART_PHONE -> {
                browserFolder = "chrome"
                executableFile = "chromedriver.exe"
                if (CommonUtils.isMac()) {
                    machineBits = "64bit"
                }
            }
            BrowserType.FIREFOX -> {
                browserFolder = "firefox"
                executableFile = "geckodriver.exe"
                machineBits = "64bit"
            }
            BrowserType.IE -> {
                browserFolder = "ie"
                executableFile = "IEDriverService.exe"
            }
            BrowserType.EDGE -> {
                browserFolder = "edge"
                executableFile = "MicrosoftWebDriver.exe"
                machineBits = "64bit"
            }
            else -> {
                // TODO: 指定されたブラウザーの種別が存在しない場合
            }
        }

        // macOSの時は「.exe」の拡張子を削除する
        if (CommonUtils.isMac()) {
            operatingSystem = "mac"
            executableFile = executableFile.replace(".exe", "")
        }

        return "$DRIVER_ROOT_PATH/$browserFolder/$operatingSystem/$machineBits/$executableFile"
    }
}