package selenide.helpers.browsers

import com.codeborne.selenide.WebDriverProvider
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.firefox.GeckoDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import selenide.helpers.BrowserType
import selenide.helpers.BrowserUtils
import java.io.File

object Firefox {
    // Standard = "/Applications/Firefox.app/Contents/MacOS/firefox-bin"
    // DeveloperEdition: "/Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox-bin"
    val BINARY_PATH: String = "/Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox-bin"

    class CustomWebDriverProvider : WebDriverProvider {
        override fun createDriver(desiredCapabilities: DesiredCapabilities?): WebDriver {
            val service = GeckoDriverService.Builder()
                    .usingDriverExecutable(File(BrowserUtils.getDriverLocation(BrowserType.FIREFOX)))
                    .usingAnyFreePort()
                    .build()

            val options = FirefoxOptions()
            options.setBinary(Firefox.BINARY_PATH)

            if (BrowserUtils.USE_HEADLESS) {
                options.setHeadless(true)
            }

            options.profile = createProfile(BrowserUtils.USE_PROXY_FLAG)

            return FirefoxDriver(service, options)
        }

        private fun createProfile(useProxy: Boolean = false): FirefoxProfile {
            val MANUAL_PROXY_CONFIG = 1
            val IGNORE_PROXY = "localhost, 127.0.0.1"

            val profile = FirefoxProfile()
            if (useProxy) {
                val proxyHost = BrowserUtils.proxyServer.host
                val proxyPort = BrowserUtils.proxyServer.port

                // Configures the same proxy for all variants
                profile.setPreference("network.proxy.type", MANUAL_PROXY_CONFIG)
                profile.setPreference("network.proxy.http", proxyHost)
                profile.setPreference("network.proxy.http_port", proxyPort)
                profile.setPreference("network.proxy.ssl", proxyHost)
                profile.setPreference("network.proxy.ssl_port", proxyPort)
                profile.setPreference("network.proxy.socks", proxyHost)
                profile.setPreference("network.proxy.socks_port", proxyPort)
                profile.setPreference("network.proxy.ftp", proxyHost)
                profile.setPreference("network.proxy.ftp_port", proxyPort)
                profile.setPreference("network.proxy.no_proxies_on", IGNORE_PROXY)
            }

            return profile
        }
    }
}