package selenide.helpers.browsers

import com.codeborne.selenide.WebDriverProvider
import org.openqa.selenium.PageLoadStrategy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeDriverService
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import selenide.helpers.BrowserType
import selenide.helpers.BrowserUtils
import java.io.File
import java.util.HashMap

object ChromeSp {
    // Standard: "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
    // Canary: "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary"
    val BINARY_PATH: String = "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary"
    val DEVICE_NAME: String = "Nexus 5X"

    class CustomWebDriverProvider : WebDriverProvider {
        override fun createDriver(desiredCapabilities: DesiredCapabilities?): WebDriver {
            val service = ChromeDriverService.Builder()
                    .usingDriverExecutable(File(BrowserUtils.getDriverLocation(BrowserType.CHROME)))
                    .usingAnyFreePort()
                    .build()

            val options = ChromeOptions()
            options.setPageLoadStrategy(PageLoadStrategy.NORMAL)

            options.setBinary(Chrome.BINARY_PATH)

            if (BrowserUtils.USE_PROXY_FLAG) {
                options.setProxy(BrowserUtils.getProxy())
            }

            if (BrowserUtils.USE_HEADLESS) {
                options.setHeadless(true)
            }

            val mobileEmulation = HashMap<String, String>()
            mobileEmulation.put("deviceName", DEVICE_NAME)
            options.setExperimentalOption("mobileEmulation", mobileEmulation)

            return ChromeDriver(service, options)
        }
    }
}