package selenide.helpers

enum class BrowserType {
    CHROME,
    FIREFOX,
    IE,
    EDGE,
    SAFARI,
    SMART_PHONE
}
